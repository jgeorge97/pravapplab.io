---
title: 'Homepage'
meta_title: 'Prav App'
description: "Prav- privacy respecting chat app"
intro_image: "/images/intro.png"
intro_image_absolute: false
intro_image_hide_on_mobile: true
---

# Prav - Privacy Respecting Chatting App 

Prav is a social project to get a lot of people to invest small amounts to run an interoperable messaging service that will respect users' [freedom](https://www.gnu.org/philosophy/free-software-even-more-important.html) and privacy. [Learn more](/about).
